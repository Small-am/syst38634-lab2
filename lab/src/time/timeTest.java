package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class timeTest {
	
	@Test
	public void testGetTotalMilliseconds()
	{
		int totalMilliseconds = time.getTotalMilliseconds("12:05:05:05");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 5);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalMillisecondsException()
	{
		int totalMilliseconds = time.getTotalMilliseconds("12:05:05:0A");
		fail("Invalid number of milliseconds");
	}
	
	@Test 
	public void testGetTotalMillisecondsBoundaryIn()
	{
		int totalMilliseconds = time.getTotalMilliseconds("12:05:05:999");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 999);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalMillisecondsBoundaryOut()
	{
		int totalMilliseconds = time.getTotalMilliseconds("12:05:05:1000");
		fail("Invalid number of milliseconds");
	}
	/*
	@Test
	public void testGetTotalSecondsRegular()
	{
		int totalSeconds = time.getTotalSeconds( "01:01:01" );
		assertTrue("The time provided deos not match the result", totalSeconds == 3661 );
	}
	
	@Test 
	public void testGetTotalSecondsBoundaryIn()
	{
		int totalSeconds = time.getTotalSeconds( "01:01:59" );
		assertTrue("The time provided deos not match the result", totalSeconds == 3719 );
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut()
	{
		int totalSeconds = time.getTotalSeconds( "01:01:60" );
		assertFalse("The time provided deos not match the result", totalSeconds == 3720 );
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsException()
	{
		int totalSeconds = time.getTotalSeconds( "01:01:0t" );
		assertFalse("The time provided deos not match the result", totalSeconds == 3661);
	}
	*/
}
